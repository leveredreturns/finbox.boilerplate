/**
 * Define and Export
 */

export default function flash (type, message, hide_ms) {
  if (typeof window !== 'undefined') {
    var dialog = require('dialog-component')
    if (typeof type === 'object') {
      message = type.message || 'notification'
      type = type.type || 'Done!'
    } else if (!message) {
      message = type
      type = 'notification'
    }

    var d = dialog(type, message)
      .effect('slide').show().fixed()
      .addClass('flash').addClass(type)
      .closable()

    if (hide_ms) d.hide(hide_ms)
  }
}
