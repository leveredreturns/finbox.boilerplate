/* global describe, it */

/**
 * Module Dependencies
 */

import { should } from 'chai'
import storage from 'storage'
import store_referral from './../lib/store-referral'

/**
 * Tests
 */

should()
describe('store-referral', () => {
  it('should transform query to `referral` with advocate token', (next) => {
    const utm_medium = 'email'
    const utm_campaign = 'alerts'
    const utm_source = 'email'
    const utm_content = 'watchlist'
    const r = 'rtoken'
    const query = {
      utm_medium,
      utm_campaign,
      utm_source,
      utm_content,
      r
    }

    const referral = store_referral({ query })

    referral.utm_medium.should.equal(utm_medium)
    referral.utm_campaign.should.equal(utm_campaign)
    referral.utm_source.should.equal(utm_source)
    referral.utm_content.should.equal(utm_content)
    referral.user_agent.should.not.be.undefined
    referral.referral_token.should.equal(r)
    referral.description.should.equal('advocate')

    next()
  })

  it('should transform query to `referral` with `iuid`', (next) => {
    const utm_medium = 'email'
    const utm_campaign = 'alerts'
    const utm_source = 'email'
    const utm_content = 'watchlist'
    const iuid = 'token-iuid'
    const query = {
      utm_medium,
      utm_campaign,
      utm_source,
      utm_content,
      iuid
    }

    const referral = store_referral({ query })

    referral.utm_medium.should.equal(utm_medium)
    referral.utm_campaign.should.equal(utm_campaign)
    referral.utm_source.should.equal(utm_source)
    referral.utm_content.should.equal(utm_content)
    referral.user_agent.should.not.be.undefined
    referral.referral_token.should.equal(iuid)
    referral.description.should.equal('invite')

    next()
  })

  it('should store token into storage', (next) => {
    const utm_medium = 'email'
    const utm_campaign = 'alerts'
    const utm_source = 'email'
    const utm_content = 'watchlist'
    const iuid = 'token-iuid'
    const query = {
      utm_medium,
      utm_campaign,
      utm_source,
      utm_content,
      iuid
    }

    store_referral({ query })

    const referral = JSON.parse(storage.getItem('referral'))
    referral.utm_medium.should.equal(utm_medium)
    referral.utm_campaign.should.equal(utm_campaign)
    referral.utm_source.should.equal(utm_source)
    referral.utm_content.should.equal(utm_content)
    referral.user_agent.should.not.be.undefined
    referral.referral_token.should.equal(iuid)
    referral.description.should.equal('invite')

    next()
  })

  it('should override only new keys', (next) => {
    const utm_medium = 'email'
    const utm_campaign = 'alerts'
    const utm_source = 'email'
    const utm_content = 'watchlist'
    const iuid = 'token-iuid'

    const query_one = {
      utm_medium,
      utm_campaign,
      utm_source,
      utm_content
    }

    store_referral({ query: query_one })

    const query_two = {
      utm_campaign: 'free cash flow',
      utm_source: 'google',
      utm_content: 'free cash flow',
      iuid
    }

    store_referral({ query: query_two })

    const referral = JSON.parse(storage.getItem('referral'))
    referral.utm_medium.should.equal(utm_medium)
    referral.utm_campaign.should.equal(query_two.utm_campaign)
    referral.utm_source.should.equal(query_two.utm_source)
    referral.utm_content.should.equal(query_two.utm_content)
    referral.user_agent.should.not.be.undefined
    referral.referral_token.should.equal(iuid)
    referral.description.should.equal('invite')

    next()
  })
})
