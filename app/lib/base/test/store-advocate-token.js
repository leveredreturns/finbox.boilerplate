/* global describe, it */

/**
 * Module Dependencies
 */

import { should } from 'chai'
import store_advocate_token from './../lib/store-advocate-token'
import storage from 'storage'

/**
 * Tests
 */

should()
describe('store-advocate-token', () => {
  it('should store advocate token passed in `r`', (next) => {
    const r = 'advocate-token'
    const query = { r }
    store_advocate_token({ query })
      .should.be.true

    storage.getItem('advocate_token').should.equal(r)
    next()
  })

  it('should store advocate token passed in `rt`', (next) => {
    const rt = 'advocate-token'
    const query = { rt }
    store_advocate_token({ query })
      .should.be.true

    storage.getItem('advocate_token').should.equal(rt)
    next()
  })

  it('should do nothing', (next) => {
    const query = {}
    store_advocate_token({ query })
      .should.be.false
    next()
  })
})
