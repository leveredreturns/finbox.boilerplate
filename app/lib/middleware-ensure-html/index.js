/**
 * Module Dependencies
 */

import mime from 'mime-types'

/**
 * Looks up the mime type and ensure
 * the request is valid
 * @param {Function} next
 * @yield {Function}
 */
export default function * ensure_html_requests (next) {
  switch (mime.lookup(this.path)) {
    case 'application/json':
    case 'text/html':
    case false:
      return yield next
    default:
      this.status = 404
  }
}
