/* global describe, it, beforeEach */

/**
 * Module Dependencies
 */

import assert from 'assert'
import storage from './../'

/**
 * Tests
 */

describe('storage', () => {
  beforeEach((next) => {
    storage.setItem('name', 'finbox.io')
    next()
  })

  it('should save data to local storage', (next) => {
    const data = JSON.stringify({ a: 1 })
    storage.setItem('hello', data)
    assert.deepEqual(
      storage.getItem('hello'),
      data
    )
    next()
  })

  it('should get data from local storage', (next) => {
    assert.equal(
      storage.getItem('name'),
      'finbox.io'
    )
    next()
  })

  it('should return null if get not found from local storage', (next) => {
    assert.equal(
      storage.getItem('asdf'),
      null
    )
    next()
  })

  it('should remove item from local storage', (next) => {
    storage.removeItem('name')

    assert.equal(
      storage.getItem('name'),
      null
    )
    next()
  })
})
