/**
 * Selects between an in-memory store
 * or the browser localStorage based
 * on availability
 */

const storage = pick_storage()
export default storage

export function pick_storage () {
  return storage_is_valid()
    ? default_storage()
    : memory_storage()
}

export function storage_is_valid () {
  if (typeof window !== 'undefined' && typeof window.localStorage === 'object') {
    try {
      const crumb = 'crumb'
      window.localStorage.setItem('localStorageTest', crumb)
      const _crumb = window.localStorage.getItem('localStorageTest')
      window.localStorage.removeItem('localStorageTest')
      return _crumb === crumb
    } catch (e) {
      return false
    }
  }
  return false
}

export function memory_storage () {
  const store = {}
  const storage = {
    getItem: (id) => {
      return store[id] || null
    },
    setItem: (id, data) => {
      store[id] = data
    },
    removeItem: (id) => {
      delete store[id]
    },
    clear: () => {
      for (let k in store) {
        delete store[k]
      }
    }
  }
  return storage
}

export function default_storage () {
  let storage = {
    getItem: (id) => {
      return window.localStorage.getItem(id)
    },
    setItem: (id, data) => {
      try {
        window.localStorage.setItem(id, data)
      } catch (e) {
        return false
      }
    },
    removeItem: (id) => {
      window.localStorage.removeItem(id)
    },
    clear: () => {
      window.localStorage.clear()
    }
  }
  return storage
}
