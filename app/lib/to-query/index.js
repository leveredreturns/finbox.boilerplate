/**
 * Module Dependencies
 */

import query from 'query-string'

/**
 * Define and Export
 */

export default function to_query (search) {
  if (!search && typeof window !== 'undefined') {
    search = window.location.search
  }

  const parsed = query.parse(search)
  return Object.keys(parsed).reduce((p, k) => {
    p[k] = parsed[k]
    return p
  }, {})
}
