/**
 * Module Dependencies
 */

const write = require('fs').writeFileSync

class PrintChunksPlugin {
  constructor (opts) {
    this._output = opts.output
  }

  apply (compiler) {
    const output = this._output
    compiler.plugin('compilation', function (compilation, params) {
      compilation.plugin('after-optimize-chunk-assets', function (chunks) {
        const summary = chunks.map(function (c) {
          return {
            id: c.id,
            name: c.name,
            includes: c.modules.map(function (m) { return m ? m.request : '' })
          }
        })
        write(output, JSON.stringify(summary, null, 2))
      })
    })
  }
}

module.exports = PrintChunksPlugin
