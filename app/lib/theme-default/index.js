module.exports = {
  // fonts
  '$font-main': "'Droid Sans', 'Roboto', 'sans-serif'",
  '$font-alt': "'Roboto', 'Roboto Mono', 'monospace'",
  '$font-icon': 'Font-Awesome',

  // colors
  '$background': '#FAFCFF',
  '$white': '#FFF',
  '$cream': '#F9FCFF',
  '$black': '#4C4C35',

  '$lighter-gray': '#EBF0F3',
  '$light-gray': '#AAABAF',
  '$gray': '#444',
  '$dark-gray': '#47525d',
  '$darker-gray': '#3d464d',
  '$gray-blue': '#A0BDC7',

  '$lighter-blue': '#EDF6FF',
  '$light-blue': '#99C9FF',
  '$blue': '#3490fa',
  '$dark-blue': '#0A7CFF',
  '$darker-blue': '#00438F',
  '$bright-dark-blue': '#1375FC',

  '$light-orange': '#F8BF63',
  '$orange': '#F5A623',
  '$dark-orange': '#D6880A',

  '$light-teal': '#83DEEC',
  '$teal': '#27C6DD',
  '$dark-teal': '#1EACC2',

  '$light-purple': '#CDADFF',
  '$purple': '#AF7DFF',
  '$dark-purple': '#8E47FF',
  '$pink': '#F75CB8',

  '$light-facebook': '#97A7C8',
  '$facebook': '#3B5998',
  '$dark-facebook': '#2D4473',
  '$light-google': '#EAA096',
  '$google': '#DD4B39',
  '$dark-google': '#C23321',
  '$light-linkedin': '#00A4F9',
  '$linkedin': '#0077B5',
  '$dark-linkedin': '#004A71',
  '$light-twitter': '#A3D5F5',
  '$twitter': '#56B4EF',
  '$dark-twitter': '#1898e9',

  '$red': '#FF6447',
  '$dark-red': '#E8554D',

  '$youtube': '#e62117',
  '$youtube-hover': '#cc181e',

  '$light-green': '#24db80',
  '$green': '#20BD6F',
  '$dark-green': '#1ba05e',
  '$darker-green': '#0A904E',
  '$yellow': '#fec01a'
}
