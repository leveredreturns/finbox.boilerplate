/**
 * Module Dependencies
 */

import 'env'
import safe_start from 'finboxio-safestart'
import envvar from 'envvar'
import is_production from 'is-production'
import Yoo from 'yoo'
import { get } from 'finboxio-dot-prop'

/**
 * Koa middleware
 */

import conditional from 'koa1-conditional-get'
import koa_error from 'koa1-error'
import etag from 'koa1-etag'
import ensure_html_requests from 'middleware-ensure-html'
import set_locals from 'middleware-locals'
import suppress_stacktrace from 'middleware-suppress-stacktrace'

/**
 * Mounts
 */

import render from './render'

/**
 * Environmnet
 */

const production = is_production()

/**
 * Yoo
 */

const yoo = Yoo(up_one(__dirname))
export default yoo

/**
 * Check to make sure all package definitions in
 * packages.json match versions in node_modules
 */

safe_start(up_two(__dirname))

/**
 * Ensure our environment variables
 * are set before doing anything
 * throw otherwise.
 */

envvar.string('PORT')

/**
 * Error handling
 */

yoo.use(koa_error())

/**
 * Test for whitelisted CORS origins
 */

const cors_rgrx = new RegExp('(^|\\.|/)' + process.env.CORS + '$', 'i')
yoo.cors({
  credentials: true,
  origin: function (ctx) {
    if (!ctx || !ctx.request || !ctx.request.headers) return false
    if (!production) return ctx.request.headers.origin
    if (cors_rgrx.test(ctx.request.headers.origin)) return ctx.request.headers.origin
  }
})

/**
 * Logging
 */

yoo.logger()
yoo.use(function * (next) {
  try {
    yield next
  } catch (err) {
    console.error('[ERROR]: ' + err.stack)
    throw err.response ? format_error(err) : err
  }
})

/**
 * Compress
 */

production && yoo.compress()

/**
 * Bundling
 */

if (!production) {
  const Webpack = require('webpack')
  const webpack = Webpack(require(up_two(__dirname) + '/webpack.dev.js'))
  const webpack_dev = require('koa-webpack-dev-middleware')
  const webpack_hot = require('koa-webpack-hot-middleware-2')
  yoo.use(webpack_dev(webpack, {
    noInfo: true,
    stats: { colors: true }
  }))

  yoo.use(webpack_hot(webpack, {
    logs: false
  }))
}

// etag support
yoo.use(conditional())
yoo.use(etag())
yoo.static('dist')
yoo.static('public')

// no asset requests should go beyond this point
yoo.use(ensure_html_requests)

/**
 * Attach locals
 */

yoo.use(set_locals)

/**
 * Skip the stack-trace for standard HTTP errors
 * (Useful for cleaner test output)
 */

process.env.SUPPRESS_LOGS && yoo.use(suppress_stacktrace([
  400,
  403,
  409
]))

/**
 * Mounts
 */

yoo.mount('/', render)

/**
 * Cluster in production
 */

var CLUSTER = envvar.string('CLUSTER', 'false')
var WORKERS = envvar.number('WORKERS', require('os').cpus().length)
production && CLUSTER === 'true' && yoo.cluster({ workers: WORKERS })

/**
 * Listen
 */

if (!module.parent) {
  yoo.listen(function () {
    var addr = this.address()
    console.log('listening on [%s]:%s', addr.address, addr.port)
  })
}

/**
 * Handle uncaught exceptions
 */

process.on('uncaughtException', function (err) {
  console.error(err.stack)
})

/**
 * Handle errors coming from koa
 */

function format_error (err) {
  return new Error(JSON.stringify({
    message: err.message,
    stack: err.stack,
    type: err.type,
    url: get(err, 'response.request.url')
  }))
}

function up_one (dir) { return dir + '/..' }
function up_two (dir) { return dir + '/../..' }
