
0.0.1-beta2 / 2017-04-27
==================

  * remove mangle uglify option
  * fix uglify console argument
  * experiment removing babel-polyfill in favor of use useBuiltIns
  * size reduction experiment
  * clean up for production
  * clean up base
  * update Dockerfile
  * fix typo in webpack config

0.0.1-beta1 / 2017-04-26
==================

- first beta release


0.0.1-beta0 / 2017-04-26
==================

- development release
