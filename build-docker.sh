#! /bin/bash

BUILD_ENV=$1
TAG=$2

if [[ "$BUILD_ENV" == "" ]]; then
  echo "Please specify a build environment."
  exit 1
fi

if [[ "$TAG" == "" ]]; then
  echo "Please specify a build tag."
  exit 1
fi

if [[ ! -e ".env.$BUILD_ENV" ]]; then
  echo "$BUILD_ENV is not a valid build environment."
  exit 1
fi

for line in $(cat ".env.$BUILD_ENV"); do
  found=$(echo $line | sed -E 's/([^=]+)=(.*)/ARG \1/' | awk "{ print \"grep \\\"\" \$1 \" \" \$2 \"\\\"\" \" Dockerfile \" }" | bash)
  line=$(echo $line | sed -E "s/([^=]+)=[\"']?([^\"']*)[\"']?/\1=\2/")
  if [[ "$found" != "" ]]; then
    build_args="$build_args --build-arg $line"
  fi
done

docker build -t $TAG --build-arg LR_NPM_TOKEN=$LR_NPM_TOKEN $build_args .
