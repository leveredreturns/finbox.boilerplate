/* eslint-disable */

/**
 * Module Dependencies
 */

const webpack = require('webpack')
const join = require('path').join
const relative = require('path').relative
const resolve = require('path').resolve
const log_update = require('log-update')
const is_uri = require('is-uri')
const theme = require('./app/lib/theme-default')

/**
 * Context
 */

const root = join(__dirname, 'app')
const NODE_PATH = process.env.NODE_PATH
const BUILD_PAGES = process.env.BUILD_PAGES
const ANALYZE_BUILD = process.env.ANALYZE_BUILD
const BUILD_DASHBOARD = process.env.BUILD_DASHBOARD
const FAKE_PRODUCTION = process.env.FAKE_PRODUCTION

/**
 * Configuration
 */

const cssimport = require('postcss-import')
const rucksack = require('rucksack-css')
const vars = require('postcss-simple-vars')
const nested = require('postcss-nested')
const url = require('postcss-url')

/**
 * Plugins
 */

const DashboardPlugin = require('webpack-dashboard/plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const WebpackNotifierPlugin = require('webpack-notifier')
const PrintChunksPlugin = require('./app/lib/webpack-print-chunks-plugin')

/**
 * Loaders
 */

const PostCSSLoader = require('postcss-loader')
const MarkdownLoader = require('markdown-loader')
const BabelLoader = require('babel-loader')
const Styleloader = require('style-loader')
const CSSLoader = require('css-loader')
const URLLoader = require('url-loader')
const FileLoader = require('file-loader')
const HTMLLoader = require('html-loader')

const rules = [
  {
    test: /\.(js|jsx)$/,
    use: [
      {
        loader: 'babel-loader',
        query: {
          cacheDirectory: true,
          babelrc: false,
          presets: [
            [
              'env',
              {
                targets: {
                  browsers: [
                    '> 20%'
                  ],
                  modules: false,
                  loose: true,
                  useBuiltIns: true
                }
              }
            ]
          ],
          plugins: [
            'transform-strict-mode',
            'transform-object-rest-spread',
            'transform-decorators-legacy'
          ]
        }
      }
    ],
    exclude: [
      /node_modules/,
      /test\//
    ],
    include: [
      join(root, 'pages'),
      join(root, 'lib')
    ]
  },
  {
    test: /\.css$/,
    use: [
      'style-loader',
      'css-loader',
      'postcss-loader'
    ]
  },
  {
    test: /\.(png|jpg|jpeg|gif|svg)$/,
    loader: 'url-loader'
  },
  {
    test: /\.(woff|woff2)/,
    loader: 'url-loader'
  },
  {
    test: /\.(ttf|eot)$/,
    loader: 'file-loader'
  },
  {
    test: /\.html$/,
    loader: 'html-loader'
  },
  {
    test: /\.(md|markdown)$/,
    use: [
      'html-loader',
      'markdown-loader'
    ]
  }
]

/**
 * Export `config`
 */

module.exports = {
  devtool: 'eval',
  resolve: {
    modules: [
      resolve(root, 'lib'),
      resolve(root, 'pages'),
      'node_modules'
    ],
    extensions: [
      '.js',
      '.jsx',
      '.json'
    ]
  },
  entry: entry(BUILD_PAGES),
  output: {
    path: join(root, 'dist'),
    filename: '[name].js',
    publicPath: '/'
  },
  module: { rules },
  watch: true,
  performance: {
    maxAssetSize: 25000,
    maxEntrypointSize: 25000
  },
  plugins: plugins(ANALYZE_BUILD, BUILD_DASHBOARD, FAKE_PRODUCTION)
}

/**
 * PostCSS
 */

function postcss () {
  const plugins = [
    cssimport({ root, path: join(root, NODE_PATH) }),
    rucksack({ autoprefixer: true }),
    nested(),
    vars({ variables: () => theme }),
    url()
  ]
  return plugins
}

/**
* On progress
*/

function on_progress (progress, message) {
  log_update('\n  < webpack >  ' + Math.round(progress * 100) + '%  :  ' + (message || 'done') + '\n')
}

function entry (build_pages) {
  const pages = {
    signup: [
      'webpack-hot-middleware/client?reload=true',
      join(root, 'pages', 'signup', 'signup.js')
    ]
  }

  if (!build_pages) return pages
  const build = build_pages.split(',')
  const _pages = Object.keys(pages).reduce((p, page) => {
    if (build.indexOf(page.toLowerCase()) !== -1) {
      p[page] = pages[page]
    }
    return p
  }, {})

  console.log('*---* webpack will only build:', build_pages, ' pages *---*')
  return _pages
}

function plugins (analyze_build, build_dashboard, fake_production) {
  const list = [
    new webpack.LoaderOptionsPlugin({
      options: {
        context: root,
        postcss: postcss()
      }
    }),
    new PrintChunksPlugin({
      output: join(__dirname, 'chuck-composition.json')
    }),
    new WebpackNotifierPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': Object.keys(process.env).reduce((o, k) => {
        o[k] = JSON.stringify(process.env[k])
        return o
      }, {})
    }),
    new webpack.ProgressPlugin(on_progress),
    new webpack.HotModuleReplacementPlugin()
  ]

  if (analyze_build) {
    list.push(
      new BundleAnalyzerPlugin({
        analyzerMode: 'server',
        analyzerPort: 8888,
        reportFilename: 'report.html',
        openAnalyzer: true,
        generateStatsFile: false,
        statsFilename: 'stats.json',
        statsOptions: null,
        logLevel: 'info'
      })
    )
  }

  if (build_dashboard) {
    list.push(
      new DashboardPlugin({ port: 3001 })
    )
  }

  if (fake_production) {
    list.push(
      new webpack.optimize.UglifyJsPlugin({
        test: /\.jsx?$/,
        minimize: true,
        compress: {
          unused: true,
          dead_code: true,
          screw_ie8: true,
          drop_console: false,
          warnings: false
        },
        output: {
          comments: false
        },
        sourceMap: true
      })
    )
  }

  return list
}
