SHELL := /bin/bash

PORT ?= 7013
WEBPACK_DEV = node node_modules/webpack/bin/webpack.js --optimize-minimize --config webpack.dev.js
WEBPACK_PROD = node --max_old_space_size=3900 node_modules/webpack/bin/webpack.js --optimize-minimize --config webpack.prod.js

NYC = NODE_PATH=app/lib BABEL_ENV=test nyc --require babel-register --require babel-polyfill --require ignore-styles --require ./app/test/setup.js --lines 80 --functions 80 mocha
MOCHA = NODE_PATH=app/lib BABEL_ENV=test node node_modules/.bin/mocha -r babel-register -r babel-polyfill -r ignore-styles
PATH_UNIT_TEST = app/lib/*/test/*.js
PATH_SERVER_TEST = app/routes/*/test/*.js

DOCKER_REGISTRY=docker.finbox.io
DOCKER_USER=finboxio
DOCKER_IMAGE=offers

GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
GIT_COMMIT := $(shell git rev-parse HEAD)
GIT_REPO := $(shell git remote -v | grep origin | grep "(fetch)" | awk '{ print $$2 }')
GIT_DIRTY := $(shell git status --porcelain | wc -l)
GIT_DIRTY := $(shell if [[ "$(GIT_DIRTY)" -gt "0" ]]; then echo "true"; else echo "false"; fi)

VERSION := $(shell git describe --abbrev=0)
VERSION_DIRTY := $(shell git log --pretty=format:%h $(VERSION)..HEAD | wc -l)

BUILD_COMMIT := $(shell if [[ "$(GIT_DIRTY)" == "true" ]]; then echo $(GIT_COMMIT)+dev; else echo $(GIT_COMMIT); fi)
BUILD_COMMIT := $(shell echo $(BUILD_COMMIT) | cut -c1-12)
BUILD_VERSION := $(shell if [[ "$(VERSION_DIRTY)" -gt "0" ]]; then echo "$(VERSION)-$(BUILD_COMMIT)"; else echo $(VERSION); fi)
BUILD_VERSION := $(shell if [[ "$(VERSION_DIRTY)" -gt "0" ]] || [[ "$(GIT_DIRTY)" == "true" ]]; then echo "$(BUILD_VERSION)-dev"; else echo $(BUILD_VERSION); fi)
BUILD_VERSION := $(shell if [[ "$(GIT_BRANCH)" != "master" ]]; then echo $(GIT_BRANCH)-$(BUILD_VERSION); else echo $(BUILD_VERSION); fi)

development:
	@cd app && NODE_PATH=lib CONFIG=development ../node_modules/.bin/node-dev -r babel-register routes/index.js

fake.production:
	@cd app && NODE_PATH=lib CONFIG=development FAKE_PRODUCTION=true ../node_modules/.bin/node-dev -r babel-register routes/index.js

analyze:
	@cd app && NODE_PATH=lib CONFIG=development ANALYZE_BUILD=true ../node_modules/.bin/node-dev -r babel-register routes/index.js

dashboard:
	@cd app && NODE_PATH=lib CONFIG=development BUILD_DASHBOARD=true ../node_modules/.bin/webpack-dashboard --port 3001 -- ../node_modules/.bin/node-dev -r babel-register routes/index.js

info:
	@echo "git branch:      $(GIT_BRANCH)"
	@echo "git commit:      $(GIT_COMMIT)"
	@echo "git repo:        $(GIT_REPO)"
	@echo "git dirty:       $(GIT_DIRTY)"
	@echo "version:         $(VERSION)"
	@echo "commits since:   $(VERSION_DIRTY)"
	@echo "build commit:    $(BUILD_COMMIT)"
	@echo "build version:   $(BUILD_VERSION)"
	@echo "docker image:    $(DOCKER_REGISTRY)/$(DOCKER_USER)/$(DOCKER_IMAGE):$(BUILD_VERSION)"

version:
	@echo $(BUILD_VERSION) | tr -d '\r' | tr -d '\n' | tr -d ' '

docker.production:
	@./build-docker.sh production $(DOCKER_REGISTRY)/$(DOCKER_USER)/$(DOCKER_IMAGE):$(BUILD_VERSION)-production
	@docker push $(DOCKER_REGISTRY)/$(DOCKER_USER)/$(DOCKER_IMAGE):$(BUILD_VERSION)-production

docker.staging:
	@./build-docker.sh staging $(DOCKER_REGISTRY)/$(DOCKER_USER)/$(DOCKER_IMAGE):$(BUILD_VERSION)-staging
	@docker push $(DOCKER_REGISTRY)/$(DOCKER_USER)/$(DOCKER_IMAGE):$(BUILD_VERSION)-staging

docker.dev: env.local
	@./node_modules/.bin/docker-compose-watch -f compose/development.yml -p finbox up

install: package.json
	@npm install
	@touch node_modules

lint:
	@./node_modules/.bin/eslint -c ./.eslintrc --ext .js,.jsx --fix .

stylelint:
	@./node_modules/.bin/stylelint 'app/**/*.css'

watch:
	@./node_modules/.bin/wtch -e js,jsx,css,jade | ./node_modules/.bin/garnish --verbose

tests:
	@$(NYC) $(PATH_UNIT_TEST) $(PATH_SERVER_TEST)

tests.unit:
	@$(MOCHA) -r ./app/test/setup.js $(PATH_UNIT_TEST)

tests.server:
	@$(MOCHA) -r ./app/test/setup.js $(PATH_SERVER_TEST)

tests.watch:
	@$(MOCHA) -r ./app/test/setup.js -w $(PATH_UNIT_TEST) $(PATH_SERVER_TEST)

tests.watch.unit:
	@$(MOCHA) -r ./app/test/setup.js -w $(PATH_UNIT_TEST)

tests.watch.server:
	@$(MOCHA) -r ./app/test/setup.js -w $(PATH_SERVER_TEST)

.PHONY: development analyze dashboard info version docker.production docker.staging docker.dev install lint stylelint watch tests tests.unit tests.server tests.watch tests.watch.unit tests.watch.server
